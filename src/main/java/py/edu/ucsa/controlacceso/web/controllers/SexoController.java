package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.Sexo;
import py.edu.ucsa.controlacceso.core.services.SexoService;

@RestController
@RequestMapping("sexo")
public class SexoController {

	@Autowired
	private SexoService sexoService;

	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Sexo>> listar() {
		List<Sexo> respuesta = sexoService.listar();
		return new ResponseEntity<List<Sexo>>(respuesta, HttpStatus.OK);
	}

	
	//Metodo GET que recibe como parametro el id del sexo para recuperar completamente el mismo
	@RequestMapping(value = "{idSexo}", method = RequestMethod.GET)
	public ResponseEntity<Sexo> listarById(@PathVariable("idSexo") Long idSexo) {
		Sexo respuesta = sexoService.listarById(idSexo);
		return new ResponseEntity<Sexo>(respuesta, HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Sexo>  insertar(@RequestBody Sexo obj){
		sexoService.insertar(obj);
		return new ResponseEntity<Sexo>(obj, HttpStatus.OK);
	}
	
	@RequestMapping(method = RequestMethod.PUT)
	public ResponseEntity<?> modificar(@RequestBody Sexo obj){
		sexoService.modificar(obj);
		ResponseEntity<Object> response = new ResponseEntity<Object>(sexoService, HttpStatus.OK);
		return response;
	}
	
	@RequestMapping(value="{idSexo}", method = RequestMethod.DELETE)
    public ResponseEntity<Object> borrar(@PathVariable("idSexo") Long idSexo){
	sexoService.eliminar(idSexo);
        	ResponseEntity<Object> response = new ResponseEntity<Object>(idSexo, HttpStatus.OK);
        	return response;
    }
	
	

}
