/**
 * 
 */
package py.edu.ucsa.controlacceso.web.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import py.edu.ucsa.controlacceso.core.domain.Participante;
import py.edu.ucsa.controlacceso.core.services.ParticipanteService;

/**
 * @author halfonso
 *
 */
@RestController
@RequestMapping("participante")
public class ParticipanteController {

	@Autowired
	private ParticipanteService participanteService;
	
	@RequestMapping(method = RequestMethod.GET)
	public ResponseEntity<List<Participante>> listar() {
		List<Participante> respuesta = participanteService.listar();
		return new ResponseEntity<List<Participante>>(respuesta, HttpStatus.OK);
	}

}
