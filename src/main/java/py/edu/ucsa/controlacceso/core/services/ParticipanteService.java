/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Participante;


/**
 * @author halfonso
 *
 */
@Service
public interface ParticipanteService {
	public List<Participante> listar();
	public Participante listarById(Long idParticipante);
    public void insertar(Participante obj);
	public void modificar(Participante obj);
	public void eliminar(long id);
}
