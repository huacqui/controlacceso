package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;
import py.edu.ucsa.controlacceso.core.domain.Sexo;


public interface SexoMapper {
	public List<Sexo> listar();
	public Sexo listarById(Long idSexo);
    public void insertar(Sexo obj);
	public void modificar(Sexo obj);
	public void eliminar(long id);
}
