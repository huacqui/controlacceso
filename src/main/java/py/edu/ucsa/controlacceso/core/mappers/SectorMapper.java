/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.Sector;

/**
 * @author halfonso
 *
 */
public interface SectorMapper {
	public List<Sector> listar();
	public Sector listarById(Long idSector);
    public void insertar(Sector obj);
	public void modificar(Sector obj);
	public void eliminar(long id);
}
