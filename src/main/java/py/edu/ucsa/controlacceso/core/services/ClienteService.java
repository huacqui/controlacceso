/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Cliente;

/**
 * @author halfonso
 *
 */
@Service
public interface ClienteService {
	public List<Cliente> listar();
	public Cliente listarById(Long idCliente);
    public void insertar(Cliente obj);
	public void modificar(Cliente obj);
	public void eliminar(long id);
	
	
}
