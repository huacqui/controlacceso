/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.Pais;

/**
 * @author halfonso
 *
 */
public interface PaisMapper {
	public List<Pais> listar();
	public Pais listarById(Long idPais);
    public void insertar(Pais obj);
	public void modificar(Pais obj);
	public void eliminar(long id);
	
	
}
