/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.EventoAsistencia;

/**
 * @author halfonso
 *
 */
public interface EventoAsistenciaMapper {
	public List<EventoAsistencia> listar();
	public EventoAsistencia listarById(Long idEventoAsistencia);
    public void insertar(EventoAsistencia obj);
	public void modificar(EventoAsistencia obj);
	public void eliminar(long id);
	
	
}
