package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Sexo;
import py.edu.ucsa.controlacceso.core.mappers.SexoMapper;
import py.edu.ucsa.controlacceso.core.services.SexoService;
@Service
public class SexoServiceImpl implements SexoService {

	@Autowired
	private SexoMapper sexoMapper;

	@Override
	public List<Sexo> listar() {
		return sexoMapper.listar();
	}

	@Override
	public Sexo listarById(Long idSexo) {		
		return sexoMapper.listarById(idSexo);
	}

	@Override
	public void insertar(Sexo obj) {
		sexoMapper.insertar(obj);		
	}

	@Override
	public void modificar(Sexo obj) {
		sexoMapper.modificar(obj);
		
	}

	@Override
	public void eliminar(long idSexo) {
		sexoMapper.eliminar(idSexo);
		
	}
	
	

}
