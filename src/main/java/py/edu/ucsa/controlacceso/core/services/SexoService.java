package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Sexo;

@Service
public interface SexoService {
	public List<Sexo> listar();
	public Sexo listarById(Long idSexo);
	public void insertar(Sexo obj);
	public void modificar(Sexo obj);
	public void eliminar(long id);
}
