package py.edu.ucsa.controlacceso.core.domain;

import java.sql.Date;

public class EventoAsistencia {

	private Integer idEventoParticipante;
	private Evento evento;
	private Participante Participante;
	private EventoSector eventoSector;
	private String activo;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private String acreditado;
	private String acr_fecha_hora;
	private String acr_usuario;
	public Integer getIdEventoParticipante() {
		return idEventoParticipante;
	}
	public void setIdEventoParticipante(Integer idEventoParticipante) {
		this.idEventoParticipante = idEventoParticipante;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	public Participante getParticipante() {
		return Participante;
	}
	public void setParticipante(Participante participante) {
		Participante = participante;
	}
	public EventoSector getEventoSector() {
		return eventoSector;
	}
	public void setEventoSector(EventoSector eventoSector) {
		this.eventoSector = eventoSector;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getAcreditado() {
		return acreditado;
	}
	public void setAcreditado(String acreditado) {
		this.acreditado = acreditado;
	}
	public String getAcr_fecha_hora() {
		return acr_fecha_hora;
	}
	public void setAcr_fecha_hora(String acr_fecha_hora) {
		this.acr_fecha_hora = acr_fecha_hora;
	}
	public String getAcr_usuario() {
		return acr_usuario;
	}
	public void setAcr_usuario(String acr_usuario) {
		this.acr_usuario = acr_usuario;
	}
	
}
