/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Pais;
import py.edu.ucsa.controlacceso.core.mappers.PaisMapper;
import py.edu.ucsa.controlacceso.core.services.PaisService;

/**
 * @author halfonso
 *
 */
@Service
public class PaisServiceImpl implements PaisService {

	@Autowired
	private PaisMapper paisMaper;
	
	
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.PaisService#listar()
	 */
	@Override
	public List<Pais> listar() {
		// TODO Auto-generated method stub
		return paisMaper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.PaisService#listarById(java.lang.Long)
	 */
	@Override
	public Pais listarById(Long idPais) {
		// TODO Auto-generated method stub
		return paisMaper.listarById(idPais);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.PaisService#insertar(py.edu.ucsa.controlacceso.core.domain.Pais)
	 */
	@Override
	public void insertar(Pais obj) {
		// TODO Auto-generated method stub
		paisMaper.insertar(obj);

	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.PaisService#modificar(py.edu.ucsa.controlacceso.core.domain.Pais)
	 */
	@Override
	public void modificar(Pais obj) {
		// TODO Auto-generated method stub
		paisMaper.modificar(obj);

	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.PaisService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		paisMaper.eliminar(id);

	}

}
