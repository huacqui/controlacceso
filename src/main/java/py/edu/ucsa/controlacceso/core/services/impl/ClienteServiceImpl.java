/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Cliente;
import py.edu.ucsa.controlacceso.core.mappers.ClienteMapper;

/**
 * @author halfonso
 *
 */
@Service
public class ClienteServiceImpl implements py.edu.ucsa.controlacceso.core.services.ClienteService {
 
	@Autowired
	private ClienteMapper clienteMapper;
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ClienteService#listar()
	 */
	@Override
	public List<Cliente> listar() {
		// TODO Auto-generated method stub
		return clienteMapper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ClienteService#listarById(java.lang.Long)
	 */
	@Override
	public Cliente listarById(Long idCliente) {
		// TODO Auto-generated method stub
		return clienteMapper.listarById(idCliente);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ClienteService#insertar(py.edu.ucsa.controlacceso.core.domain.Cliente)
	 */
	@Override
	public void insertar(Cliente obj) {
		// TODO Auto-generated method stub
		clienteMapper.insertar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ClienteService#modificar(py.edu.ucsa.controlacceso.core.domain.Cliente)
	 */
	@Override
	public void modificar(Cliente obj) {
		// TODO Auto-generated method stub
		clienteMapper.modificar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.ClienteService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		clienteMapper.eliminar(id);
	}

}
