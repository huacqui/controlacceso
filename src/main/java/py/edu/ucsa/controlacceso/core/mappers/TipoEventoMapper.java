/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;
import py.edu.ucsa.controlacceso.core.domain.TipoEvento;

/**
 * @author halfonso
 *
 */
public interface TipoEventoMapper {
	public List<TipoEvento> listar();
	public TipoEvento listarById(Long idTipoEvento);
    public void insertar(TipoEvento obj);
	public void modificar(TipoEvento obj);
	public void eliminar(long id);
}
