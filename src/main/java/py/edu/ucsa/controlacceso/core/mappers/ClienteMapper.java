/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.Cliente;

/**
 * @author halfonso
 *
 */
public interface ClienteMapper {
	public List<Cliente> listar();
	public Cliente listarById(Long idCliente);
    public void insertar(Cliente obj);
	public void modificar(Cliente obj);
	public void eliminar(long id);
	
	
}
