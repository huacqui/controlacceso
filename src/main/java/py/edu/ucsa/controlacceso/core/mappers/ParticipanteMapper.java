/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.Participante;


/**
 * @author halfonso
 *
 */
public interface ParticipanteMapper {
	public List<Participante> listar();
	public Participante listarById(Long idParticipante);
    public void insertar(Participante obj);
	public void modificar(Participante obj);
	public void eliminar(long id);
}
