/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.EventoParticipante;

/**
 * @author halfonso
 *
 */
public interface EventoParticipanteMapper {
	public List<EventoParticipante> listar();
	public EventoParticipante listarById(Long idEventoParticipante);
    public void insertar(EventoParticipante obj);
	public void modificar(EventoParticipante obj);
	public void eliminar(long id);
	
	
}
