/**
 * 
 */
package py.edu.ucsa.controlacceso.core.mappers;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.EventoSector;

/**
 * @author halfonso
 *
 */
public interface EventoSectorMapper {
	public List<EventoSector> listar();
	public EventoSector listarById(Long idEventoSector);
    public void insertar(EventoSector obj);
	public void modificar(EventoSector obj);
	public void eliminar(long id);
	
	
}
