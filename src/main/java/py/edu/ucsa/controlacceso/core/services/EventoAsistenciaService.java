/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.EventoAsistencia;

/**
 * @author halfonso
 *
 */
@Service
public interface EventoAsistenciaService {
	public List<EventoAsistencia> listar();
	public EventoAsistencia listarById(Long idEventoAsistencia);
    public void insertar(EventoAsistencia obj);
	public void modificar(EventoAsistencia obj);
	public void eliminar(long id);
	
	
}
