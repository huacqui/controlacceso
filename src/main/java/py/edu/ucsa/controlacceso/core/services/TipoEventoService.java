/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.TipoEvento;

/**
 * @author halfonso
 *
 */
@Service
public interface TipoEventoService {
	public List<TipoEvento> listar();
	public TipoEvento listarById(Long idTipoEvento);
    public void insertar(TipoEvento obj);
	public void modificar(TipoEvento obj);
	public void eliminar(long id);
}
