/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.Evento;
import py.edu.ucsa.controlacceso.core.mappers.EventoMapper;
import py.edu.ucsa.controlacceso.core.services.EventoService;

/**
 * @author halfonso
 *
 */
@Service
public class EventoServiceImpl implements EventoService {

	@Autowired
	private EventoMapper eventoMapper;
	
	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EventoService#listar()
	 */
	@Override
	public List<Evento> listar() {
		// TODO Auto-generated method stub
		return eventoMapper.listar();
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EventoService#listarById(java.lang.Long)
	 */
	@Override
	public Evento listarById(Long idEvento) {
		// TODO Auto-generated method stub
		return  eventoMapper.listarById(idEvento);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EventoService#insertar(py.edu.ucsa.controlacceso.core.domain.Evento)
	 */
	@Override
	public void insertar(Evento obj) {
		// TODO Auto-generated method stub
		eventoMapper.insertar(obj);
		
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EventoService#modificar(py.edu.ucsa.controlacceso.core.domain.Evento)
	 */
	@Override
	public void modificar(Evento obj) {
		// TODO Auto-generated method stub
		eventoMapper.modificar(obj);
	}

	/* (non-Javadoc)
	 * @see py.edu.ucsa.controlacceso.core.services.EventoService#eliminar(long)
	 */
	@Override
	public void eliminar(long id) {
		// TODO Auto-generated method stub
		eventoMapper.eliminar(id);
	}
	
	

}
