/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.SubSector;

/**
 * @author halfonso
 *
 */
@Service
public interface SubSectorService {
	public List<SubSector> listar();
	public SubSector listarById(Long idSubSector);
    public void insertar(SubSector obj);
	public void modificar(SubSector obj);
	public void eliminar(long id);
}
