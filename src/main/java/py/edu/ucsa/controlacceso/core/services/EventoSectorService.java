/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import org.springframework.stereotype.Service;

import py.edu.ucsa.controlacceso.core.domain.EventoSector;

/**
 * @author halfonso
 *
 */
@Service
public interface EventoSectorService {
	public List<EventoSector> listar();
	public EventoSector listarById(Long idEventoSector);
    public void insertar(EventoSector obj);
	public void modificar(EventoSector obj);
	public void eliminar(long id);
	
	
}
