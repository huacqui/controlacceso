/**
 * 
 */
package py.edu.ucsa.controlacceso.core.services;

import java.util.List;

import py.edu.ucsa.controlacceso.core.domain.Evento;

/**
 * @author halfonso
 *
 */
public interface EventoService {
	public List<Evento> listar();
	public Evento listarById(Long idEvento);
    public void insertar(Evento obj);
	public void modificar(Evento obj);
	public void eliminar(long id);
}
