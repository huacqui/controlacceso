package py.edu.ucsa.controlacceso.core.domain;

import java.sql.Date;

public class EventoParticipante {
	private Integer idEventoParticipante;
	private Evento evento;
	private Participante participante;
	private EventoSector eventoSector;
	private String activo;
	private Date fechaCreacion;
	private String usuarioCreacion;
	private String acreditado;
	private Date acrFechaHora;
	private String acrUsuario;
	
	public Integer getIdEventoParticipante() {
		return idEventoParticipante;
	}
	public void setIdEventoParticipante(Integer idEventoParticipante) {
		this.idEventoParticipante = idEventoParticipante;
	}
	public Evento getEvento() {
		return evento;
	}
	public void setEvento(Evento evento) {
		this.evento = evento;
	}
	public Participante getParticipante() {
		return participante;
	}
	public void setParticipante(Participante participante) {
		this.participante = participante;
	}
	public EventoSector getEventoSector() {
		return eventoSector;
	}
	public void setEventoSector(EventoSector eventoSector) {
		this.eventoSector = eventoSector;
	}
	public String getActivo() {
		return activo;
	}
	public void setActivo(String activo) {
		this.activo = activo;
	}
	public Date getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	public String getAcreditado() {
		return acreditado;
	}
	public void setAcreditado(String acreditado) {
		this.acreditado = acreditado;
	}
	public Date getAcrFechaHora() {
		return acrFechaHora;
	}
	public void setAcrFechaHora(Date acrFechaHora) {
		this.acrFechaHora = acrFechaHora;
	}
	public String getAcrUsuario() {
		return acrUsuario;
	}
	public void setAcrUsuario(String acrUsuario) {
		this.acrUsuario = acrUsuario;
	}

}
